import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import 'normalize.css'
import axios from 'axios'

const app = createApp(App);
app.use(router, axios).mount('#app')

